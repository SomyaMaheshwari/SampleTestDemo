package com.example.koa.unittest;

import com.example.koa.MyClass;
import junit.framework.Assert;
import org.junit.Test;


public class MyClassTest {

    @Test
    public void TestKey() {
        MyClass myClass = new MyClass();
        myClass.setId(2);
        myClass.setSecret(5);
        myClass.setName("First key");

        Assert.assertEquals(10,myClass.calculateKey().intValue());
    }
}
