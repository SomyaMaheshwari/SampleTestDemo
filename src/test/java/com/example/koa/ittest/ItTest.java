package com.example.koa.ittest;

import com.example.koa.Lock;
import com.example.koa.MyClass;
import junit.framework.Assert;
import org.junit.Test;


public class ItTest {
    @Test
    public void integration(){

        MyClass myClass=new MyClass();
        myClass.setId(2);
        myClass.setName("Testing key");
        myClass.setSecret(5);

        Lock lock=new Lock(10);

        Assert.assertEquals(true,lock.isLocked());
        Assert.assertEquals(true,lock.unlock(myClass.calculateKey()));
        Assert.assertEquals(false,lock.isLocked());

        lock.lock();

        myClass.setSecret(6);
        Assert.assertEquals(false,lock.unlock(myClass.calculateKey()));
        Assert.assertEquals(true,lock.isLocked());
    }


}
